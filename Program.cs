﻿using System;
using System.Collections.Generic;
public enum Val
{
    Zero,
    One, 
    Two, 
    Three, 
    Four, 
    Five, 
    Six, 
    Seven, 
    Eight,
    Bomb
}

namespace Minesweeper
{
    class Program
    {
        static public void Main(String[] args)
        {
            Console.WriteLine("Enter a board - (Width, Height, Mines)");
            string[] boardSpecs = Console.ReadLine().Split(',');

            State state = null;
            try
            {
                state = new State(int.Parse(boardSpecs[0].Trim()), int.Parse(boardSpecs[1].Trim()), int.Parse(boardSpecs[2].Trim()));
            }
            catch (System.FormatException){}
            finally
            {
                state = new State(16,30,99);
            }

            Console.WriteLine("Controls: Arrow keys to move cursor, D to dig, F to flag. (Press any key to begin)");
            Console.ReadKey();
            state.show();
            while (!state.dead)
            {
                ConsoleKey key = Console.ReadKey().Key;
                
                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        state.cursor = new Coordinate(Math.Max(0, state.cursor.X - 1), state.cursor.Y);
                        break;
                    case ConsoleKey.LeftArrow:
                        state.cursor = new Coordinate(state.cursor.X, Math.Max(0, state.cursor.Y - 1));
                        break;
                    case ConsoleKey.RightArrow:
                        state.cursor = new Coordinate(state.cursor.X, Math.Min(state.Size.Y - 1, state.cursor.Y + 1));
                        break;
                    case ConsoleKey.DownArrow:
                        state.cursor = new Coordinate(Math.Min(state.Size.X - 1, state.cursor.X + 1), state.cursor.Y);
                        break;
                    case ConsoleKey.D:
                        state.dig(state.cursor);
                        break;
                    case ConsoleKey.F:
                        state.flag(state.cursor);
                        break;
                    default:
                        break;
                }


                state.show();

                if (state.cellsLeft == 0)
                {
                    break;
                }
            }

            if (state.dead)
            {
                state.show(true);
            }
        }
    }



    public class State
    {
        public State(int xin, int yin, int mine)
        {
            undug = true;
            Mines = mine;
            dead = false;

            for (int i = 0; i < xin; i++)
            {
                for (int j = 0; j < yin; j++)
                {
                    Grid.Add(new Coordinate(i, j), new CellState());
                }
            }

            cursor = new Coordinate(0, 0);

            Size = new Coordinate (xin, yin);

            cellsLeft = xin * yin - Mines;
        }
        public Dictionary<Coordinate, CellState> Grid = new Dictionary<Coordinate, CellState>();
        public Coordinate cursor;
        public static int Mines;
        public Coordinate Size;
        public int cellsLeft;
        public bool undug;
        public bool dead;
        public void flag(Coordinate cell)
        {
            if (!Grid[cell].dug)
            {
                bool flagged = Grid[cell].flagged;
                Grid[cell].flagged = !Grid[cell].flagged;

                foreach (Coordinate adjacentCell in Adjacent(cell))
                {
                    try
                    {
                        if(flagged)
                        {
                            Grid[adjacentCell].adjacentFlags--;
                        }
                        else
                        {
                            Grid[adjacentCell].adjacentFlags++;
                        }
                        if (Grid[adjacentCell].dug)
                        {
                            dig(adjacentCell);
                        }
                    }
                    catch(KeyNotFoundException){}
                    catch(ArgumentOutOfRangeException){}
                }
            }
        }
        public List<Coordinate> Adjacent(Coordinate cell)
        {
            List<Coordinate> output = new List<Coordinate>();
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    try
                    {
                        Coordinate adjacentCell = new Coordinate(cell.X + x, cell.Y + y);
                        output.Add(adjacentCell);
                    }
                    catch(KeyNotFoundException){}
                    catch(ArgumentOutOfRangeException){}
                }
            }
            output.Remove(cell);
            return output;
        }
        public void dig(Coordinate cell)
        {
            if (undug)
            {
                generate();
                undug = false;
                dig(cell);
            }
            else if (Grid[cell].dug)
            {                
                if (Grid[cell].adjacentFlags == ((int)Grid[cell].val) && ((int)Grid[cell].val) != 0 && !Grid[cell].complete)
                {
                    Grid[cell].complete = true;
                    foreach (Coordinate adjacentCell in Adjacent(cell))
                    {
                        try
                        {
                            dig(adjacentCell);
                        }
                        catch(KeyNotFoundException){}
                        catch(ArgumentOutOfRangeException){}
                    }
                }
            }
            else if (Grid[cell].flagged){}
            else if (Grid[cell].val == Val.Bomb)
            {
                Grid[cell].dug = true;
                dead = true;
            }
            else
            {
                int bombs = 0;
                foreach (Coordinate adjacentCell in Adjacent(cell))
                {
                    try
                    {
                        if(Grid[adjacentCell].val == Val.Bomb)
                        {
                            bombs++;
                        }
                    }
                    catch(KeyNotFoundException){}
                    catch(ArgumentOutOfRangeException){}

                }

                Grid[cell].val = (Val)bombs;
                
                cellsLeft--;

                Grid[cell].dug = true;

                if (Grid[cell].adjacentFlags >= ((int)Grid[cell].val))
                {
                    foreach (Coordinate adjacentCell in Adjacent(cell))
                    {
                        try
                        {
                            dig(adjacentCell);
                        }
                        catch(KeyNotFoundException){}
                        catch(ArgumentOutOfRangeException){}
                    }
                }
            }
        }
        public void generate()
        {
            var coords = new List<Coordinate>(Grid.Keys);
    
            coords.Remove(new Coordinate (cursor.X+1, cursor.Y+1));
            coords.Remove(new Coordinate (cursor.X+1, cursor.Y));
            coords.Remove(new Coordinate (cursor.X+1, cursor.Y-1));
            coords.Remove(new Coordinate (cursor.X, cursor.Y+1));
            coords.Remove(new Coordinate (cursor.X, cursor.Y));
            coords.Remove(new Coordinate (cursor.X, cursor.Y-1));
            coords.Remove(new Coordinate (cursor.X-1, cursor.Y+1));
            coords.Remove(new Coordinate (cursor.X-1, cursor.Y));
            coords.Remove(new Coordinate (cursor.X-1, cursor.Y-1));

            Random rng = new Random();
            int n = coords.Count;  
            while (n > 1) 
            {  
                n--;  
                int k = rng.Next(n + 1);  
                var value = coords[k];  
                coords[k] = coords[n];  
                coords[n] = value;  
            } 
            try{coords.RemoveRange(0, coords.Count - Mines);}catch(ArgumentOutOfRangeException){}

            foreach (KeyValuePair<Coordinate, CellState> cell in Grid)
            {
                if (coords.Contains(cell.Key))
                {
                    cell.Value.val = Val.Bomb;
                }
            }
        }
        public void show()
        {
            string output = "\x1b[2J\x1b[H";
            int currentX = 0;
            foreach (KeyValuePair<Coordinate, CellState> cell in Grid)
            {
                if (currentX != cell.Key.X)
                {
                    output += "\n";
                }

                currentX = cell.Key.X;

                if (cell.Value.Equals(cursor))
                {
                    output += "\x1b[90";
                }

                if (cursor.Equals(cell.Key))
                {
                    output += "\x1b[100m";
                }

                if (cell.Value.flagged)
                {
                    output += "\x1b[91m" + '⚑' + "\x1b[0m ";
                    continue;
                }

                if (!cell.Value.dug)
                {
                    output += "\x1b[90m" + '•' + "\x1b[0m ";
                    continue;
                }
                
                if (cell.Value.complete)
                {
                    output += " " + "\x1b[0m ";
                    continue;
                }

                if (cell.Value.adjacentFlags == ((int)cell.Value.val))
                {
                    switch (cell.Value.val)
                    {
                        case Val.Zero:
                            output += ' ' + "\x1b[0m ";
                            break; 
                        case Val.One:
                            output += "\x1b[90m" + '1' + "\x1b[0m ";
                            break;
                        case Val.Two:
                            output += "\x1b[90m" + '2' + "\x1b[0m ";
                            break;
                        case Val.Three:
                            output += "\x1b[90m" + '3' + "\x1b[0m ";
                            break;
                        case Val.Four:
                            output += "\x1b[90m" + '4' + "\x1b[0m ";
                            break;
                        case Val.Five:
                            output += "\x1b[90m" + '5' + "\x1b[0m ";
                            break;
                        case Val.Six:
                            output += "\x1b[90m" + '6' + "\x1b[0m ";
                            break;
                        case Val.Seven:
                            output += "\x1b[90m" + '7' + "\x1b[0m ";
                            break;
                        case Val.Eight:
                            output += "\x1b[90m" + '8' + "\x1b[0m ";
                            break;
                        case Val.Bomb:
                            output += 'X' + "\x1b[0m ";
                            break;
                    }
                    continue;
                }

                switch (cell.Value.val)
                {
                    case Val.Zero:
                        output += ' ' + "\x1b[0m ";
                        break;
                    case Val.One:
                        output += "\x1b[34;1m" + '1' + "\x1b[0m ";
                        break;
                    case Val.Two:
                        output += "\x1b[32;1m" + '2' + "\x1b[0m ";
                        break;
                    case Val.Three:
                        output += "\x1b[31;1m" + '3' + "\x1b[0m ";
                        break;
                    case Val.Four:
                        output += "\x1b[35;1m" + '4' + "\x1b[0m ";
                        break;
                    case Val.Five:
                        output += "\x1b[33;1m" + '5' + "\x1b[0m ";
                        break;
                    case Val.Six:
                        output += "\x1b[36;1m" + '6' + "\x1b[0m ";
                        break;
                    case Val.Seven:
                        output += "\x1b[93;1m" + '7' + "\x1b[0m ";
                        break;
                    case Val.Eight:
                        output += "\x1b[95;1m" + '8' + "\x1b[0m ";
                        break;
                    default:
                        break;
                    case Val.Bomb:
                        output += 'X' + "\x1b[0m ";
                        break;
                }

            }

            Console.Write(output + "\n");
        }

        public void show(bool dead)
        {
            string output = "\x1b[2J\x1b[H";
            int currentX = 0;
            foreach (KeyValuePair<Coordinate, CellState> cell in Grid)
            {
                if (currentX != cell.Key.X)
                {
                    output += "\n";
                }

                currentX = cell.Key.X;

                if (cell.Value.Equals(cursor))
                {
                    output += "\x1b[90";
                }

                if (cursor.Equals(cell.Key))
                {
                    output += "\x1b[100m";
                }

                if (cell.Value.flagged)
                {
                    output += "\x1b[91m" + '⚑' + "\x1b[0m ";
                    continue;
                }

                if (!cell.Value.dug && cell.Value.val != Val.Bomb)
                {
                    output += "\x1b[90m" + '•' + "\x1b[0m ";
                    continue;
                }
                
                if (cell.Value.complete)
                {
                    output += " " + "\x1b[0m ";
                    continue;
                }

                if (cell.Value.adjacentFlags == ((int)cell.Value.val))
                {
                    switch (cell.Value.val)
                    {
                        case Val.Zero:
                            output += ' ' + "\x1b[0m ";
                            break; 
                        case Val.One:
                            output += "\x1b[90m" + '1' + "\x1b[0m ";
                            break;
                        case Val.Two:
                            output += "\x1b[90m" + '2' + "\x1b[0m ";
                            break;
                        case Val.Three:
                            output += "\x1b[90m" + '3' + "\x1b[0m ";
                            break;
                        case Val.Four:
                            output += "\x1b[90m" + '4' + "\x1b[0m ";
                            break;
                        case Val.Five:
                            output += "\x1b[90m" + '5' + "\x1b[0m ";
                            break;
                        case Val.Six:
                            output += "\x1b[90m" + '6' + "\x1b[0m ";
                            break;
                        case Val.Seven:
                            output += "\x1b[90m" + '7' + "\x1b[0m ";
                            break;
                        case Val.Eight:
                            output += "\x1b[90m" + '8' + "\x1b[0m ";
                            break;
                        case Val.Bomb:
                            output += 'X' + "\x1b[0m ";
                            break;
                    }
                    continue;
                }

                switch (cell.Value.val)
                {
                    case Val.Zero:
                        output += ' ' + "\x1b[0m ";
                        break;
                    case Val.One:
                        output += "\x1b[34;1m" + '1' + "\x1b[0m ";
                        break;
                    case Val.Two:
                        output += "\x1b[32;1m" + '2' + "\x1b[0m ";
                        break;
                    case Val.Three:
                        output += "\x1b[31;1m" + '3' + "\x1b[0m ";
                        break;
                    case Val.Four:
                        output += "\x1b[35;1m" + '4' + "\x1b[0m ";
                        break;
                    case Val.Five:
                        output += "\x1b[33;1m" + '5' + "\x1b[0m ";
                        break;
                    case Val.Six:
                        output += "\x1b[36;1m" + '6' + "\x1b[0m ";
                        break;
                    case Val.Seven:
                        output += "\x1b[93;1m" + '7' + "\x1b[0m ";
                        break;
                    case Val.Eight:
                        output += "\x1b[95;1m" + '8' + "\x1b[0m ";
                        break;
                    default:
                        break;
                    case Val.Bomb:
                        output += "\x1b[93;2;1m" + 'X' + "\x1b[0m ";
                        break;
                }

            }

            Console.Write(output + "\n");
        }

    }
    public class Coordinate
    {

        public Coordinate(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; }
        public int Y { get; }

        public override bool Equals(object obj)
        {
            return obj is Coordinate coordinate &&
                   X == coordinate.X &&
                   Y == coordinate.Y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }
    }
    public class CellState
    {
        public CellState()
        {
            val = Val.Zero;
            flagged = false;
            dug = false;
            adjacentFlags = 0;
            complete = false;
            
        }
        public int adjacentFlags;
        public Val val;
        public bool flagged;
        public bool dug;
        public bool complete;
    }
}
/* Pseudocode Plan 
    Get grid size
    Generate grid filled with Zero cells
    Populate grid with bombs (10% of cells)
    Loop{
        Print
        Get Key
        if arrow{
            Move cursor by arrow
            Change highlighted cell
            Continue
        }
        if F{
            if character isn't '•'{
                Continue
            }
            Set character to F for cell
            Continue
        }
        if D{
            if character isn't '•'{
                Contine
            }

            Set character to number of adjacent bombs of cell
            if character > 8{
                Set Character to X for cell
                Break
            }
        }
    }
    reveal all bombs
    end
    
*/